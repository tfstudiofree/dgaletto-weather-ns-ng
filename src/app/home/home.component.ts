import { Component, OnInit } from "@angular/core";
import { DarkskyService } from "../shared/services/darksky.service";
import { ConnectivityService } from "../shared/services/connectivity.service";
import { finalize } from "rxjs/operators";
import { GeolocationService } from "../shared/services/geolocation.service";
import { registerElement } from "nativescript-angular/element-registry";
import { GeocodeService } from "../shared/services/geocode.service";
import * as Toast from 'nativescript-toast';

registerElement("PullToRefresh", () => require("nativescript-pulltorefresh").PullToRefresh);

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    public weather: Darksky;
    public locationName: string; 
    public location: any;
    public error: string;
    public isBusy = true;
    private pullRefresh: any;

    constructor(
        private darksky: DarkskyService,
        private connectivityService: ConnectivityService,
        private geolocationService: GeolocationService,
        private geocodeService: GeocodeService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.showHelp();
        this.getLocation();
    }

    getWeather(latitude: number, longitude: number) {

        if(this.connectivityService.isConnected){
            this.darksky.getData(latitude, longitude)
            .pipe(
                finalize(() => this.pullRefresh.refreshing = false)
            )
            .subscribe((location) => {
                this.weather = location as Darksky;
                this.isBusy = false;
            }, (error) => {
                console.log(error);
            });
        } else {
            this.isBusy = false;
            this.error = 'Not connected. Pull to refresh';
            Toast.makeText("Not connected").show();
        }
        
    }

    refreshWeather(event?) {
        this.pullRefresh = event.object;
        this.getLocation();
    }

    getLocation() {
        this.geolocationService.getLocationOnce().then((location: any) => {
            this.location = location;
            this.getLocationName(location.latitude, location.longitude);
            this.getWeather(location.latitude, location.longitude);
        }).catch( () => {
            this.isBusy = false;
            this.error = 'Please, go to settings and turn on location. Then reopen app.';
        });
    }

    getLocationName(latitude: number, longitude: number) {
        this.geocodeService.getAddress(latitude, longitude).subscribe( (address: any) => {
            this.locationName = this.geocodeService.getCityState(address);
        }, (error) => {
            this.locationName = "Undefined location";
        });
    }

    showHelp() {
        Toast.makeText("Pull to refresh").show();
    }

    roundData(value) {
        return Math.round(value);
    }

    toPercent(value) {
        return this.roundData(value*100) + '%';
    }
}
