import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Config } from "../config";

@Injectable()
export class DarkskyService {
    private serverUrl = "https://api.darksky.net/forecast";

    constructor(private http: HttpClient) { }

    getData(latitude: number, longitude:number) {
        return this.http.get(`${this.serverUrl}/${Config.darkskyKey}/${latitude},${longitude}?units=si`);
    }

}