import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Config } from "../config";

@Injectable()
export class GeocodeService {

    private serverUrl = "https://maps.googleapis.com/maps/api/geocode/json";

    constructor(private http: HttpClient) { }

    getAddress(latitude: number, longitude:number) {
        return this.http.get(`${this.serverUrl}?latlng=${latitude},${longitude}&key=${Config.gmapsKey}`);
    }

    getCoordinates(address: string){
        return this.http.get(`${this.serverUrl}?address=${address}&key=${Config.gmapsKey}`);
    }

    getCityState(address) {
        console.dir(JSON.stringify(address));
        if (address.results[0]) {
            let results = address.results;
            let city = '';
            let country = '';
            
            
            for(let i = 0; i < results[0].address_components.length; i++){
                if (results[0].address_components[i].types[0] === "locality") {
                    city = results[0].address_components[i].long_name;
                }
                
                if (results[0].address_components[i].types[0] === "country") {
                    country = results[0].address_components[i].long_name;
                }
            }
            return city +' - ' + country;
        }else {
            return 'Undefined location';
        }
    }

}