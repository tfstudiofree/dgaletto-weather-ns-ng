import { Injectable } from '@angular/core';
import { connectionType as ConnectionType, getConnectionType } from 'connectivity'

@Injectable()
export class ConnectivityService {

	constructor() { }

	getConnectionType(): ConnectionType {
		return getConnectionType();
	}

	isConnected() {
		var connection: ConnectionType = this.getConnectionType();
		return connection != ConnectionType.none
    }
}