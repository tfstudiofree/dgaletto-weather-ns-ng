import { Injectable } from "@angular/core";
import { getCurrentLocation, isEnabled, enableLocationRequest } from "nativescript-geolocation";
import { Accuracy } from "tns-core-modules/ui/enums";

@Injectable()
export class GeolocationService {

    public distanceResult: string = "0";
    public distance: number = 0;
    public index: number = 0;
    
    public latitude: number = -31.4135;
    public longitude: number = -64.18105;

    constructor() { }

    public getLocationPermission() {

        return new Promise( (resolve, reject) =>{
            isEnabled().then(function (isEnabled) {
                if (!isEnabled) {
                    enableLocationRequest().then( () => {
                        resolve(true);
                    }, (error)  => {
                        reject();
                    });
                } else {
                    resolve(true);
                }
            }, function (error) {
                reject();
            });
        });
    }

    public getLocationOnce(): Promise<any> {
    
        return new Promise((resolve, reject) => {
    
            getCurrentLocation({
                desiredAccuracy: Accuracy.any,
                timeout: 5000
            })
                .then(location => {
                    this.latitude = location.latitude;
                    this.longitude = location.longitude;

                    resolve({ 'latitude': this.latitude, 'longitude': this.longitude });
                }).catch(error => {
                    resolve( { 'latitude': this.latitude, 'longitude': this.longitude });
                });
        });
        
    }

}