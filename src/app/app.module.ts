import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { DarkskyService } from "./shared/services/darksky.service";
import { ConnectivityService } from "./shared/services/connectivity.service";
import { GeolocationService } from "./shared/services/geolocation.service";
import { GeocodeService } from "./shared/services/geocode.service";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptHttpClientModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        DarkskyService,
        ConnectivityService,
        GeolocationService,
        GeocodeService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
